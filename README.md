# M2101 TP Sujet 4

Nom des étudiants du groupe :
MORICARD William;
ZARFANI Mehdi

But du programme : 
Le but de ce programme en langage C, et de pouvoir chiffrer ou déchiffrer des messages avec les chiffres de VIgenères et de Cesar.
La clef sera choisi par l'utilisateur que cela soit pour chiffrer ou déchiffrer un programme.
Le but n'est pas de retrouver la bonne clef pour un message codé et un chiffrement donné par l'utilisateur.
A la fin du programme l'utilisateur pourra enregistré son résultat dans un fichier de son choix.

Les fonctions :
Il y en a huit et le programme principal:
 
* int verifierAlphanumerique(wchar_t * message) :
        Prend un message en entrée sous forme d'une chaîne de wide characters et vérifie si il ne comporte pas de caractères spéciaux(selon un liste fini de caractères).
        Si il en comporte retourne 0 sinon 1
* void convertirAccents(wchar_t * message):
        Prend un message toujours sous forme d'une chaîne de wide characters en entrée et n'a pas de sortie
        Cet fonction va modifier le message pour enlever tous les accents compris sur des voyelles
* void viderBuffer():
        A pour but de vider le buffer et ainsi ne pas avoir d'erreur lors de la saisie de caractères.
* void convertirMin(wchar_t * message):
        Prend un message en entrée sous forme d'une chaîne de wide characters et ne renvoie rien
        Transforme un message pour le rendre uniquement en minuscules
* void chiffrerVigenere(wchar_t * clef, wchar_t * message):
        Prend un message et une clef sous forme de chaînes de wide characters ne renvoie rien.
        Cet fonction modifie le message pour le chiffrer par la clef de Vigenères
* void dechiffrerVigenere(wchar_t * clef, wchar_t * message):
        Prend un message et une clef sous forme de chaînes de wide characters ne renvoie rien.
        Cet fonction modifie un message codé par vigenère pour le déchiffrer grâce à la clef
* void chiffrerCesar(int clef, wchar_t * message):
        Prend un message sous forme d'une chaînes wide characters et un clef sous forme d'integer, ne renvoie rien.
        Cet fonction modifie le message pour le chiffrer par la clef de Cesar
* void dechiffrerCesar(int clef, wchar_t * message):
        Prend un message sous forme d'une chaînes wide characters et un clef sous forme d'integer, ne renvoie rien
        Cet fonction modifie le message codé par Cesar pour le déchiffrer grâce à la clef
