SOURCES = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SOURCES})
GCC = gcc
all:main 

main: ${BINAIRES}
	${GCC} $^ -o $@

clean: main
	rm main *.o

%.o: %.c %.h
	${GCC} -c $<