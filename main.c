#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <string.h>

#define TAILLE_MAX 501
#define TAILLE_MAX_NFIC 51

/*  fonction qui vérifie si un charactere est alphanumérique ou non
	renvoie 1 si il l'est
	sinon renvoie 0
*/
int verifierAlphanumerique(wchar_t * message);

/*  procedure qui enlève tous les accents des voyelles d'une chaîne de caractères
	modifie la chaîne
*/
void convertirAccents(wchar_t * message);

/*   fonction qui vide le buffer si nécessaire pour les saisies */
void viderBuffer();

/*   procedure qui convertit en minuscule*/
void convertirMin(wchar_t * message);

/*	 procedure qui créé un message codée par la clé de vigenère
	 a besoin de la clé en wchar_t et du message à coder en wchar_t
	 transforme le message en message codé
*/
void chiffrerVigenere(wchar_t * clef, wchar_t * message);

/*	 procedure qui décode un message codée par la clé de vigenère
	 a besoin de la clé en wchar_t et du message à décoder en wchar_t
	 transforme le message codé en message clair
*/
void dechiffrerVigenere(wchar_t * clef, wchar_t * message);

/*	 procedure qui créé un message codée par la clé de césar
	 a besoin de la clé en integer et du message à coder en wchar_t
	 transforme le message en message codé
*/
void chiffrerCesar(int clef, wchar_t * message);

/* procedure qui décode un message chiffré par la clef de César
	a besoin de la clef utilisé pour coder le message (int) et du message codé en wchar_t
	transforme le message
*/
void dechiffrerCesar(int clef, wchar_t * message);

int main(void){
	setlocale(LC_ALL,"");
	
	/*On créé les variables qui seront utiles dans le programmes*/
	wchar_t choix[] = L"N";
	wchar_t message[TAILLE_MAX];
	int choixCle;
	int choixChiffreOuDechiffre;
	
	/*On enregistre ce que veut l'utilisateur*/
		
	/*On demande que veut faire l'utilisateur l'utilsateur veut utiliser*/
	wprintf(L"Vos clefs et message à décoder doivent au maximum 500 caractères (espaces compris)\n");
	wprintf(L"Quelle chiffrement voulez-vous utiliser ?\n(Veuillez répondre par le chiffre correspondant à la ligne)\nChoix :\n");
	wprintf(L"1.   Chiffre de Vigenère\n");
	wprintf(L"2.   Chiffre de César\n");
		
	/*On enregistre le choix de l'utilisateur*/
	wscanf(L"%d",&choixCle);
		
	while(choixCle != 1 && choixCle != 2){
		wprintf(L"Il semblerait que vous n'ayez rentré un choix invalide, veuillez saisir un choix valide.\n");
		wprintf(L"1.   Chiffre de Vigenère\n");
		wprintf(L"2.   Chiffre de César\n");
		wscanf(L"%d",&choixCle);
	}
		
	/*On demande à l'utilisateur si il veut chiffrer ou dechiffrer un message*/
	wprintf(L"Voulez vous chiffre ou déchiffrer un message ?\n");
	wprintf(L"1.   Chiffrer\n");
	wprintf(L"2.   Déchiffrer\n");
		
	/*On enregistre le choix de l'utilisateur*/
	wscanf(L"%d",&choixChiffreOuDechiffre);
		
	while(choixChiffreOuDechiffre != 1 && choixChiffreOuDechiffre != 2){
		wprintf(L"Il semblerait que vous n'ayez rentré un choix invalide, veuillez saisir un choix valide.\n");
		wprintf(L"1.   Chiffrer\n");
		wprintf(L"2.   Déchiffrer\n");
		wscanf(L"%d",&choixChiffreOuDechiffre);
	}
		
	/*On enregistre la chaîne de l'utilisateur*/
	while(choix[0] != 'o'){
		wprintf(L"Quelle est la chaîne de caractères à chiffrer\\déchiffrer ?\n");
		wscanf(L"%ls",message);
		viderBuffer();
		wprintf(L"Message : %ls\n",message);
		
			
		/*On vérifie que la chaîne est en alphanumérique et on redemande une chaîne jusqu'à ce qu'elle le soit*/
		while(verifierAlphanumerique(message) == 0){
			wprintf(L"Votre message comporte des caractères spéciaux veuillez le retaper :\n");
			wscanf(L"%ls",message);
			viderBuffer();
		}
		/*Sinon c'est forcément en alphanumérique*/
		
		/*On enlève les accents*/
		convertirAccents(message);
		
		/*On met le message en minuscule pour pouvoir le coder plus facilement*/
		convertirMin(message);
		
		/*On vérifie que l'utilisateur à rentrer le bon message*/
		wprintf(L"Vous avez entré %ls (message sans majuscules ni accents)? \n(tapez O ou o pour oui, et n'importe quoi d'autres pour non)\n",message);
		wscanf(L"%ls",choix);
		viderBuffer();
		convertirMin(choix);
	}
	
	/*On demande la clef et chiffre le message de l'utilisateur*/
	switch(choixChiffreOuDechiffre){
		case 1 : ;
			switch(choixCle){
				case 1:	; /*Chiffre de Vigenère*/
					wchar_t clef[TAILLE_MAX];
					wprintf(L"Votre clef doit être une chaîne de caractères :");
					wscanf(L"%ls",clef);
					chiffrerVigenere(clef,message);
					wprintf(L"Votre message chiffré : %ls\n",message);
					break;
				case 2:	; /*Chiffre de César*/
					int cle = 0;
					wprintf(L"Votre clef doit être un entier :");
					wscanf(L"%d",&cle);
					chiffrerCesar(cle,message);
					wprintf(L"Votre message chiffré : %ls\n", message);
					break;
				default:
					printf("Erreur dans les choix\n");
					break;
			}
			break;
		case 2 : ;
			switch(choixCle){
				case 1:	; /*Chiffre de Vigenère*/
					wchar_t clef[TAILLE_MAX];
					wprintf(L"Votre clef doit être une chaîne de caractères :");
					wscanf(L"%ls",clef);
					dechiffrerVigenere(clef,message);
					wprintf(L"Votre message déchiffré : %ls\n",message);
					break;
				case 2:	; /*Chiffre de César*/
					int cle = 0;
					wprintf(L"Votre clef doit être un entier :");
					wscanf(L"%d",&cle);
					dechiffrerCesar(cle,message);
					wprintf(L"Votre message déchiffré : %ls\n", message);
					break;
				default:
					printf("Erreur dans les choix\n");
					break;
			}
			break;
		default : 
			wprintf(L"Erreur dans les choix\n");
			break;
	}
	
	/*On demande si l'utilisateur veut enregistrer le résultat dans un fichier*/
	wprintf(L"Voulez vous enregistrer le résultat dans un fichier ?\n(tapez O ou o pour oui, et n'importe quoi d'autres pour non)\n");
	wscanf(L"%ls",choix);
	viderBuffer();
	convertirMin(choix);
	if(choix[0] == 'o'){
		FILE* fichier = NULL;
		char nomFic[TAILLE_MAX_NFIC];
		wprintf(L"Quel est le nom du fichier (avec extension), avec moins de %d caractères :\n");
		wscanf(L"%s",nomFic);
		fichier = fopen(nomFic, "w");
		fwprintf(fichier, L"%ls\n", message);
		fclose(fichier);
	}
	
	/*Fin du programme principal*/
	return 0;
}

int verifierAlphanumerique(wchar_t * message){
	wchar_t characteresSpeciaux[] = L"><#'()[]\\{}/:;.?,!§%*µ$£¤^¨°+=~²@";
	for(int i = 0; i < wcslen(message); i++){
		for(int j = 0; j < wcslen(characteresSpeciaux); j++){
			if(message[i] == characteresSpeciaux[j]){
				return 0;
			}
		}
	}
	return 1;
}

void convertirAccents(wchar_t * message){
	/*On convertira les accents uniquement sur les voyelles */
	wchar_t accentsA[] = L"áàâäãÁÀÂÄÃ";
	wchar_t accentsE[] = L"éèêëÉÈÊË";
	wchar_t accentsI[] = L"íìîïÍÌÎÏ";
	wchar_t accentsO[] = L"óòôöÓÒÔÖ";
	wchar_t accentsU[] = L"úùûüÚÙÛÜ";
	for(int i = 0; i < wcslen(message); i++){
		for(int j = 0; j < wcslen(accentsA); j++){
			if(message[i] == accentsA[j]){
				message[i] = 'a';
			}
		}
		for(int j = 0; j < wcslen(accentsE); j++){
			if(message[i] == accentsE[j]){
				message[i] = 'e';
			}
		}
		for(int j = 0; j < wcslen(accentsI); j++){
			if(message[i] == accentsI[j]){
				message[i] = 'i';
			}
		}
		for(int j = 0; j < wcslen(accentsO); j++){
			if(message[i] == accentsO[j]){
				message[i] = 'o';
			}
		}
		for(int j = 0; j < wcslen(accentsU); j++){
			if(message[i] == accentsU[j]){
				message[i] = 'u';
			}
		}
	}
}

void viderBuffer(){
    int c;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

void convertirMin(wchar_t * message){
	for(int i = 0; i < wcslen(message); i++){
		if(message[i] >= 'A' && message[i] <= 'Z'){
			message[i] += 'a' - 'A';
		}
	}
}

void chiffrerVigenere(wchar_t * clef, wchar_t * message){
	for(int i = 0; i < wcslen(message);){
		for(int j = 0; j < wcslen(clef)&&i < wcslen(message); j++,i++){
			if((((message[i]-'a') - (clef[j]-'a'))%26)>= 0){
				message[i] = (((clef[j]-'a')+(message[i]-'a'))%26)+'a';
			}else if(((((clef[j]-'a')+(message[i]-'a'))%26 + 26)+'a')=='{'){ 
				message[i] = 'a';
			}else{
				message[i] = (((clef[j]-'a')+(message[i]-'a'))%26 +26 )+'a';
			}
			
		}
	}
}

void dechiffrerVigenere(wchar_t * clef, wchar_t * message){
	for(int i = 0; i < wcslen(message);){
		for(int j = 0; j < wcslen(clef)&&i < wcslen(message); j++,i++){
			if((((message[i]-'a') - (clef[j]-'a'))%26)>= 0){
				message[i] = (((message[i]-'a') - (clef[j]-'a'))%26)+'a';
			}else if(((((message[i]-'a') - (clef[j]-'a'))%26 + 26)+'a')=='{'){
				message[i] = 'a';
			}else{
				message[i] = (((message[i]-'a') - (clef[j]-'a'))%26 + 26)+'a';
			}
		}
	}
}

void chiffrerCesar(int clef, wchar_t * message){
	for(int i = 0; i < wcslen(message);i++){
		if(((message[i] - 'a')+clef)%26){
			message[i] = (((message[i] - 'a')+clef)%26)+'a';
		}else if(((((message[i] - 'a')+clef)%26 + 26)+'a' ) == '{'){
			message[i] = 'a';
		}else{
			message[i] = (((message[i] - 'a')+clef)%26 + 26)+'a';
		}
	}
}

void dechiffrerCesar(int clef, wchar_t * message){
	for(int i = 0; i < wcslen(message);i++){
		if((((message[i] - 'a')-clef)%26)>= 0){
			message[i] = (((message[i] - 'a')-clef)%26) + 'a';
		}else if(((((message[i] - 'a')-clef)%26 + 26)+'a') == '{'){
			message[i] = 'a';
		}else{
			message[i] = (((message[i] - 'a')-clef)%26 + 26) + 'a';
		}
	}
}
